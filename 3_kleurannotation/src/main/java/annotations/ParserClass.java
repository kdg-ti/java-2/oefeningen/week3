package annotations;

import java.lang.reflect.Method;

public class ParserClass {
    public static void execute(Class<?> aClass) {
        try {
            Object anObject = aClass.getDeclaredConstructor().newInstance();

            for (Method method : aClass.getDeclaredMethods()) {
                Kleur kleurAnnot = method.getAnnotation(Kleur.class);
                if (kleurAnnot != null) {
                    method.invoke(anObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}


