package annotations;

public class Demo {
    public void demoMethode() {

        MijnKlasse mijnKlasse = new MijnKlasse();
        // Toon alle annotaties voor de klasse be.kdg.opgave.MijnKlasse.
        toonClassAnnotations(mijnKlasse);
        // Toon de waarden van de Class-annotaties
        toonWaardenClassAnnotation(mijnKlasse);
        // Toon alle annotaties voor demoMethode uit be.kdg.opgave.MijnKlasse .
        toonMethodeAnnotations(mijnKlasse);
        // Toon alle waarden van de Methode-annotaties uit be.kdg.opgave.MijnKlasse
        toonWaardenMethodeAnnotation(mijnKlasse);
    }

    private void toonClassAnnotations(MijnKlasse mijnKlasse) {

    }

    private void toonWaardenClassAnnotation(MijnKlasse mijnKlasse) {

    }

    private void toonMethodeAnnotations(MijnKlasse mijnKlasse) {

    }

    private void toonWaardenMethodeAnnotation(MijnKlasse mijnKlasse) {

    }






}